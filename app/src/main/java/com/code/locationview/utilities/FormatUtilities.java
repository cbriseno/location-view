package com.code.locationview.utilities;

import android.location.Address;
import android.support.annotation.NonNull;

/**
 * Created by carlos on 18/05/17.
 */

public final class FormatUtilities {

    private FormatUtilities() {}

    public static String getFormattedAddress(@NonNull Address address){
        StringBuilder builder = new StringBuilder();

        if (address.getAddressLine(0) != null){
            builder.append(address.getAddressLine(0));
            builder.append(' ');
        }

        if (address.getLocality() != null){
            builder.append(address.getLocality());
            builder.append(' ');
        }

        if (address.getAdminArea() != null){
            builder.append(address.getAdminArea());
            builder.append(' ');
        }

        if (address.getCountryName() != null){
            builder.append(address.getCountryName());
            builder.append(' ');
        }

        if (address.getPostalCode() != null){

            if (builder.length() > 0) {
                builder.append(',');
            }

            builder.append(address.getPostalCode());
        }

        return builder.toString();
    }
}
