package com.code.locationview.utilities;

import android.content.Context;

/**
 * Created by carlos on 18/05/17.
 */

public final class DeviceUtilities {

    public DeviceUtilities() {}

    public static int convertToPx(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;

        return (int) (dp * scale + 0.5f);
    }

}
