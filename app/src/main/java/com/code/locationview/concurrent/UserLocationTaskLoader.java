package com.code.locationview.concurrent;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.code.locationview.persistence.UserLocation;
import com.code.locationview.persistence.UserLocationRepository;

import java.util.ArrayList;

/**
 * Created by carlos on 18/05/17.
 *
 * A class for reading {@link UserLocation} list in background
 *
 */
public class UserLocationTaskLoader extends AsyncTaskLoader<ArrayList<UserLocation>> {

    public UserLocationTaskLoader(Context context) {
        super(context);
    }

    @Override
    public ArrayList<UserLocation> loadInBackground() {
        ArrayList<UserLocation> userLocations = new ArrayList<>();

        userLocations.addAll(UserLocationRepository.getAll());

        return userLocations;
    }
}
