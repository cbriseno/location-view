package com.code.locationview.interfaces;

import com.code.locationview.persistence.UserLocation;

/**
 * Created by carlos on 18/05/17.
 */

public interface OnUserLocationGeneratedListener {

    void onUserLocationGenerated(UserLocation userLocation);

}
