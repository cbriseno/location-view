package com.code.locationview.views;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code.locationview.R;
import com.code.locationview.persistence.UserLocation;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by carlos on 18/05/17.<br><br>
 *
 * View representation of {@link UserLocation}
 *
 */

public class UserLocationRowView extends LinearLayout{

    private TextView tvRowId, tvAddress, tvDate;
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy kk:mm:ss", Locale.getDefault());

    public UserLocationRowView(Context context) {
        super(context);

        initViews();
    }

    private void initViews(){
        View vLayout = inflate(getContext(), R.layout.row_user_location, this);

        tvRowId = (TextView) vLayout.findViewById(R.id.tvRowId);
        tvAddress = (TextView) vLayout.findViewById(R.id.tvAddress);
        tvDate = (TextView) vLayout.findViewById(R.id.tvDate);
    }

    public void bind(UserLocation userLocation){
        tvRowId.setText(String.format(Locale.getDefault(), "#%d", userLocation.getId()));
        tvAddress.setText(userLocation.getAddress());
        tvDate.setText(simpleDateFormat.format(userLocation.getDate()));
    }

}
