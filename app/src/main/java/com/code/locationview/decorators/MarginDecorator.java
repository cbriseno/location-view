package com.code.locationview.decorators;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by carlos on 18/05/17.
 *
 * A simple item decorator which adds margin values.
 */

public class MarginDecorator extends RecyclerView.ItemDecoration {
    private int margin;

    public MarginDecorator(int margin) {
        this.margin = margin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);

        if(position == state.getItemCount()-1){
            outRect.bottom = margin;
            outRect.top = margin*2;
        }else if(position == 0){
            outRect.top = margin;
            outRect.bottom = margin*2;
        }else if (position % 2 == 0){
            outRect.bottom = margin*2;
            outRect.top = margin*2;
        }else{
            outRect.bottom = margin;
            outRect.top = margin;
        }

        outRect.left = margin;
        outRect.right = margin;
    }
}
