package com.code.locationview;

import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.code.locationview.concurrent.UserLocationTaskLoader;
import com.code.locationview.fragments.GeocodeFragment;
import com.code.locationview.fragments.LocationPermissionFragment;
import com.code.locationview.fragments.LocationReaderFragment;
import com.code.locationview.fragments.UserLocationListFragment;
import com.code.locationview.persistence.UserLocation;
import com.code.locationview.utilities.FormatUtilities;

import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements
        LocationPermissionFragment.OnLocationPermissionListener,
        LocationReaderFragment.OnLocationChangedListener,
        GeocodeFragment.OnReverseGeocodeListener,
        LoaderManager.LoaderCallbacks<ArrayList<UserLocation>>{

    //region Variables

    //region Constants

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String LOCATION_FRAGMENT_TAG = "LOCATION_FRAGMENT_TAG";
    private static final String LOCATION_READER_FRAGMENT_TAG = "LOCATION_READER_FRAGMENT_TAG";
    private static final String GEOCODE_FRAGMENT_TAG = "GEOCODE_FRAGMENT_TAG";
    private static final String USER_LOCATION_LIST_FRAGMENT_TAG = "USER_LOCATION_LIST_FRAGMENT_TAG";

    private static final String LAST_KNOWN_LOCATION = "LAST_KNOWN_LOCATION";

    //endregion

    //region Primitives

    private boolean isPermissionForLocationGranted;

    //endregion

    //region Objects

    private LocationPermissionFragment locationPermissionFragment;
    private LocationReaderFragment locationReaderFragment;
    private GeocodeFragment geocodeFragment;
    private UserLocationListFragment userLocationListFragment;
    private Location lastKnownLocation;

    //endregion

    //region Views [Empty region]
    //endregion

    //endregion

    //region Component life cycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null){
            locationPermissionFragment = LocationPermissionFragment.newInstance();
            locationReaderFragment = LocationReaderFragment.newInstance();
            geocodeFragment = GeocodeFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(locationPermissionFragment, LOCATION_FRAGMENT_TAG)
                    .add(locationReaderFragment, LOCATION_READER_FRAGMENT_TAG)
                    .add(geocodeFragment, GEOCODE_FRAGMENT_TAG)
                    .commit();
        }else{
            locationPermissionFragment = (LocationPermissionFragment) getSupportFragmentManager().findFragmentByTag(LOCATION_FRAGMENT_TAG);
            locationReaderFragment = (LocationReaderFragment) getSupportFragmentManager().findFragmentByTag(LOCATION_READER_FRAGMENT_TAG);
            geocodeFragment = (GeocodeFragment)getSupportFragmentManager().findFragmentByTag(GEOCODE_FRAGMENT_TAG);
            userLocationListFragment = (UserLocationListFragment) getSupportFragmentManager().findFragmentByTag(USER_LOCATION_LIST_FRAGMENT_TAG);

            lastKnownLocation = savedInstanceState.getParcelable(LAST_KNOWN_LOCATION);
        }

        locationPermissionFragment.setOnLocationPermissionListener(this);
        locationReaderFragment.setOnLocationChangedListener(this);
        geocodeFragment.setOnReverseGeocodeListener(this);

        Loader loader = getSupportLoaderManager().initLoader(0, null, this);

        if (!loader.isStarted()) {
            loader.forceLoad();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(LAST_KNOWN_LOCATION, lastKnownLocation);
    }

    //endregion

    //region Methods

    //region Events [Empty region]

    //region Application logic [Empty region]

    //endregion

    //region Rest [Empty region]

    //endregion

    //endregion

    //region UI events [Empty region]
    //endregion

    //region Actionbar clicks [Empty region]
    //endregion

    //region Private [Empty region]
    //endregion

    //region Protected [Empty region]
    //endregion

    //region Public [Empty region]

    //region Getters [Empty region]
    //endregion

    //region Setters [Empty region]
    //endregion

    //endregion

    //region Super override [Empty region]
    //endregion

    //region Interface implementations

    //region LocationPermissionFragment.OnLocationPermissionListener

    @Override
    public void onPermissionGranted() {
        isPermissionForLocationGranted = true;
    }

    @Override
    public void onPermissionDenied() {
        isPermissionForLocationGranted = false;
    }

    //endregion

    //region LocationReaderFragment.OnLocationChangedListener

    @Override
    public void onLocationChanged(Location location) {
        lastKnownLocation = location;
        geocodeFragment.reverseGeocode(location.getLatitude(), location.getLongitude());
    }

    //endregion

    //region GeocodeFragment.OnReverseGeocodeListener

    @Override
    public void onReverseGeocode(Address address) {
        if (address != null) {
            UserLocation userLocation = new UserLocation();

            userLocation.setLatitude(lastKnownLocation.getLatitude());
            userLocation.setLongitude(lastKnownLocation.getLongitude());
            userLocation.setDate(new Date());
            userLocation.setAddress(FormatUtilities.getFormattedAddress(address));
            userLocationListFragment.onUserLocationGenerated(userLocation);

            userLocation.save();
        }
    }

    //endregion

    //region LoaderManager.LoaderCallbacks<ArrayList<UserLocation>>

    @Override
    public Loader<ArrayList<UserLocation>> onCreateLoader(int id, Bundle args) {
        return new UserLocationTaskLoader(getApplicationContext());
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<UserLocation>> loader, final ArrayList<UserLocation> data) {
        if (userLocationListFragment == null) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    userLocationListFragment = UserLocationListFragment.newInstance(data);

                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.flyFragmentContainer, userLocationListFragment, USER_LOCATION_LIST_FRAGMENT_TAG)
                            .commit();

                    if (isPermissionForLocationGranted && lastKnownLocation == null) {
                        locationReaderFragment.startReadingLocation();
                    }
                }
            });
        }else if (lastKnownLocation == null){
            locationReaderFragment.startReadingLocation();
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<UserLocation>> loader) {

    }

    //endregion

    //endregion

    //endregion

    //region Inner definitions [Empty region]

    //region Interfaces [Empty region]

    //endregion

    //region Classes [Empty region]
    //endregion

    //endregion

}
