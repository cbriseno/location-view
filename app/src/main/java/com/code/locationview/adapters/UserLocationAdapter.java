package com.code.locationview.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.code.locationview.views.UserLocationRowView;
import com.code.locationview.persistence.UserLocation;

import java.util.ArrayList;

/**
 * Created by carlos on 18/05/17.
 */

public class UserLocationAdapter extends RecyclerView.Adapter<UserLocationAdapter.UserLocationViewHolder>{

    private ArrayList<UserLocation> userLocationList;
    private Context context;

    public UserLocationAdapter(Context context) {
        this.userLocationList = new ArrayList<>();
        this.context = context;
    }

    public ArrayList<UserLocation> getItems() {
        return userLocationList;
    }

    @Override
    public UserLocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserLocationViewHolder(new UserLocationRowView(context));
    }

    @Override
    public void onBindViewHolder(UserLocationViewHolder holder, int position) {
        holder.getView().bind(userLocationList.get(position));
    }

    @Override
    public int getItemCount() {
        return userLocationList.size();
    }

    class UserLocationViewHolder extends RecyclerView.ViewHolder{

        private View view;

        UserLocationViewHolder(UserLocationRowView itemView) {
            super(itemView);
            this.view = itemView;
        }

        UserLocationRowView getView(){
            return (UserLocationRowView) view;
        }

    }
}
