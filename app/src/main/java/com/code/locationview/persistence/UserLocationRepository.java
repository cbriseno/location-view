package com.code.locationview.persistence;

import com.code.locationview.persistence.UserLocation;
import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by carlos on 18/05/17.
 *
 *  Helper class which is used to access stored data
 *
 */

public final class UserLocationRepository {

    public static List<UserLocation> getAll(){
        return SugarRecord.listAll(UserLocation.class, "ID DESC");
    }

}
