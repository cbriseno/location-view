package com.code.locationview.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.code.locationview.decorators.MarginDecorator;
import com.code.locationview.R;
import com.code.locationview.adapters.UserLocationAdapter;
import com.code.locationview.interfaces.OnUserLocationGeneratedListener;
import com.code.locationview.persistence.UserLocationRepository;
import com.code.locationview.persistence.UserLocation;
import com.code.locationview.utilities.DeviceUtilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by carlos on 18/05/17.
 */

public class UserLocationListFragment extends Fragment implements OnUserLocationGeneratedListener{

    private static final String EXTRA_USER_LOCATION_LIST = "EXTRA_USER_LOCATION_LIST";

    private RecyclerView rvUserLocations;
    private UserLocationAdapter adapter;

    public static UserLocationListFragment newInstance(ArrayList<UserLocation> userLocationList){
        UserLocationListFragment fragment = new UserLocationListFragment();
        Bundle bundle = new Bundle();

        bundle.putParcelableArrayList(EXTRA_USER_LOCATION_LIST, userLocationList);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.fragment_user_location_list, container, false);
        rvUserLocations = (RecyclerView) vLayout.findViewById(R.id.rvUserLocations);


        adapter = new UserLocationAdapter(getContext());

        adapter.getItems().addAll(getExtraUserLocationList());

        rvUserLocations.addItemDecoration(new MarginDecorator(DeviceUtilities.convertToPx(getContext(), 10)));
        rvUserLocations.setAdapter(adapter);
        rvUserLocations.setLayoutManager(new LinearLayoutManager(getContext()));

        return vLayout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        getArguments().putParcelableArrayList(EXTRA_USER_LOCATION_LIST, adapter.getItems());
    }

    private ArrayList<UserLocation> getExtraUserLocationList(){
        return getArguments().getParcelableArrayList(EXTRA_USER_LOCATION_LIST);
    }

    @Override
    public void onUserLocationGenerated(UserLocation userLocation) {
        adapter.getItems().add(0, userLocation);
        adapter.notifyItemInserted(0);
        rvUserLocations.scrollToPosition(0);
    }
}
