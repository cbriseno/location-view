package com.code.locationview.fragments;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by carlos on 17/05/17.
 *
 * A fragment which can geocode a given latitude and longitude in background.<br>
 * Use its {@link GeocodeFragment.OnReverseGeocodeListener OnReverseGeocodeListener} interface to receive a callback.
 */
public class GeocodeFragment extends Fragment{

    //region Variables

    //region Constants [Empty region]
    //endregion

    //region Primitives [Empty region]
    //endregion

    //region Objects

    private OnReverseGeocodeListener onReverseGeocodeListener;
    private GeocodeTask geocodeTask;

    //endregion

    //region Views [Empty region]
    //endregion

    //endregion

    //region Component life cycle

    public static GeocodeFragment newInstance(){
        return new GeocodeFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (geocodeTask != null){
            geocodeTask.cancel(true);
        }
    }

    //endregion

    //region Methods

    //region Events [Empty region]

    //region Application logic [Empty region]

    //endregion

    //region Rest [Empty region]

    //endregion

    //endregion

    //region UI events [Empty region]
    //endregion

    //region Actionbar clicks [Empty region]
    //endregion

    //region Private [Empty region]
    //endregion

    //region Protected [Empty region]
    //endregion

    //region Public

    //region Getters [Empty region]
    //endregion

    //region Setters

    public void setOnReverseGeocodeListener(OnReverseGeocodeListener onReverseGeocodeListener) {
        this.onReverseGeocodeListener = onReverseGeocodeListener;
    }

    //endregion

    public void reverseGeocode(double latitude, double longitude){
        geocodeTask = new GeocodeTask(latitude, longitude);

        geocodeTask.execute();
    }

    //endregion

    //region Super override [Empty region]
    //endregion

    //region Interface implementations [Empty region]

    //region example-onClickListener
    //endregion

    //region example-onLocationListener
    //endregion

    //region example-onCustomActionListener
    //endregion

    //endregion

    //endregion

    //region Inner definitions

    //region Interfaces

    public interface OnReverseGeocodeListener{
        void onReverseGeocode(Address address);
    }

    //endregion

    //region Classes

    private class GeocodeTask extends AsyncTask<Void, Void, Address>{

        double latitude, longitude;

        GeocodeTask(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        protected Address doInBackground(Void... params) {
            Address address = null;

            try {
                Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);

                if (addressList != null && addressList.size() > 0) {
                    address = addressList.get(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return address;
        }

        @Override
        protected void onPostExecute(Address address) {
            super.onPostExecute(address);

            geocodeTask = null;

            if (!isCancelled() && onReverseGeocodeListener != null) {
                onReverseGeocodeListener.onReverseGeocode(address);
            }
        }
    }

    //endregion

    //endregion

}
