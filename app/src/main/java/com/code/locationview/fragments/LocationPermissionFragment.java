package com.code.locationview.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;

/**
 * Created by carlos on 18/05/17.<br><br>
 *
 * A fragment class which handles permission request for getting {@link android.Manifest.permission#ACCESS_COARSE_LOCATION} and {@link android.Manifest.permission#ACCESS_FINE_LOCATION}<br>
 * You can receive a callback by implementing its {@link OnLocationPermissionListener} interface.
 */

public class LocationPermissionFragment extends Fragment{

    private static final int PERMISSION_REQUEST_CODE = 95;
    private OnLocationPermissionListener onLocationPermissionListener;

    public static LocationPermissionFragment newInstance(){
        return new LocationPermissionFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        boolean hasCoarseLocationPermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean hasFineLocationPermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (onLocationPermissionListener != null) {
            if (hasCoarseLocationPermission && hasFineLocationPermission){
                onLocationPermissionListener.onPermissionGranted();
            }else{
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (onLocationPermissionListener != null && requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                onLocationPermissionListener.onPermissionGranted();
            }else {
                onLocationPermissionListener.onPermissionDenied();
            }
        }
    }

    public void setOnLocationPermissionListener(OnLocationPermissionListener onLocationPermissionListener) {
        this.onLocationPermissionListener = onLocationPermissionListener;
    }

    public interface OnLocationPermissionListener{
        void onPermissionGranted();
        void onPermissionDenied();
    }
}
