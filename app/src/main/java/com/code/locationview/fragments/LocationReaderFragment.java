package com.code.locationview.fragments;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by carlos on 17/05/17.<br><br>
 *
 * A fragment class which is able to request user's current location.<br>
 * By using this fragment you can easily get a callback when there is a new location update, just implement its {@link OnLocationChangedListener} interface.
 *
 */

@SuppressWarnings("MissingPermission")
public class LocationReaderFragment extends Fragment implements LocationListener{

    //region Variables

    //region Constants

    private static final String TAG = LocationReaderFragment.class.getSimpleName();

    //endregion

    //region Primitives

    private String provider;
    private boolean isRequestingLocationUpdates;
    private boolean earlyInitialization;

    //endregion

    //region Objects

    private LocationManager locationManager;
    private OnLocationChangedListener onLocationChangedListener;

    //endregion

    //region Views [Empty region]
    //endregion

    //endregion

    //region Component life cycle

    public static LocationReaderFragment newInstance(){
        return new LocationReaderFragment();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (earlyInitialization) {
            earlyInitialization = false;

            init();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (isRequestingLocationUpdates) {
            removeLocationUpdates();
        }
    }

    //endregion

    //region Methods

    //region Events [Empty region]

    //region Application logic [Empty region]

    //endregion

    //region Rest [Empty region]

    //endregion

    //endregion

    //region UI events [Empty region]
    //endregion

    //region Actionbar clicks [Empty region]
    //endregion

    //region Private

    private void init(){
        Criteria criteria =  new Criteria();
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        provider = locationManager.getBestProvider(criteria, false);
        Log.i(TAG, "Initial provider: "+provider);

        if (provider != null){
            Location location = locationManager.getLastKnownLocation(provider);

            if (location != null && (System.currentTimeMillis() - location.getTime()) < 5000) {
                Log.i(TAG, "Reusing location");
                onLocationChanged(location);
            }else{
                Log.i(TAG, "Requesting new location");
                requestLocationUpdates();
            }
        }
    }

    private void requestLocationUpdates(){
        if (locationManager != null) {
            isRequestingLocationUpdates = true;
            locationManager.requestLocationUpdates(provider, 0, 0, this);
        }
    }

    private void removeLocationUpdates(){
        if (locationManager != null) {
            isRequestingLocationUpdates = false;
            locationManager.removeUpdates(this);
        }
    }

    //endregion

    //region Protected [Empty region]
    //endregion

    //region Public

    //region Getters [Empty region]
    //endregion

    //region Setters

    public void setOnLocationChangedListener(OnLocationChangedListener onLocationChangedListener) {
        this.onLocationChangedListener = onLocationChangedListener;
    }

    //endregion

    public void startReadingLocation(){

        if (isAdded()){
            init();
        }else{
            earlyInitialization = true;
        }
    }

    //endregion

    //region Super override [Empty region]
    //endregion

    //region Interface implementations

    //region LocationListener

    @Override
    public void onLocationChanged(Location location) {
        removeLocationUpdates();

        Log.i(TAG, "New location received");
        if (onLocationChangedListener != null) {
            onLocationChangedListener.onLocationChanged(location);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i(TAG, "onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.i(TAG, "New provider enabled: "+provider);
        this.provider = provider;
        requestLocationUpdates();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i(TAG, "Provider disabled: "+provider);
    }

    //endregion

    //endregion

    //endregion

    //region Inner definitions

    //region Interfaces

    public interface OnLocationChangedListener{
        void onLocationChanged(Location location);
    }

    //endregion

    //region Classes [Empty region]
    //endregion

    //endregion

}
